#!/bin/sh

echo "Add new user..."
read user
useradd -m -s /bin/mksh -U -G wheel,disk,lp,audio,video,optical,storage,scanner,network,plugdev,xbuilder $user

echo "Change user password..."
passwd $user

